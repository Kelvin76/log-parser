# Log parser with concurrency

This program first implements the log parser described in the paper "Logram: Efficient Log Parsing Using n-Gram Dictionaries" with Rust. I then tried to improve the performance of the parser by trying 2 strategies on top of the original implementation. Both strategies are about Hashem. The first strategy is to allow multiple threads to run concurrently on the same hashmap. The second strategy is to use a concurrent hashmap implementation. The details and results of these two strategies are described in the following sections.

In other words, the focus of this project is to practice programming for concurrency with Rust, while also coming up with something meaningful.

# Introduction to the program

Most code lives in parser.rs. A bit of code is in main.rs.

You can run cargo test to run the test cases.

Here's how you can invoke the program itself.

```
cargo run --release -- --raw-spark data/from_paper.log --to-parse "17/06/09 20:11:11 INFO storage.BlockManager: Found block rdd_42_20 locally" --before "split: hdfs://hostname/2kSOSP.log:29168+7292" --after "Found block" --cutoff 3
cargo run --release -- --raw-linux data/Linux_2k.log --to-parse "Jun 23 23:30:05 combo sshd(pam_unix)[26190]: authentication failure; logname= uid=0 euid=0 tty=NODEVssh ruser= rhost=218.22.3.51  user=root" --before "rhost=<*> user=root" --after "session opened" --cutoff 100
cargo run --release -- --raw-hdfs data/HDFS_2k.log --to-parse "081109 204925 673 INFO dfs.DataNode$DataXceiver: Receiving block blk_-5623176793330377570 src: /10.251.75.228:53725 dest: /10.251.75.228:50010" --before "size <*>" --after "BLOCK* NameSystem.allocateBlock:"
cargo run --release -- --raw-hpc data/HPC_2k.log --to-parse "inconsistent nodesets node-31 0x1fffffffe <ok> node-0 0xfffffffe <ok> node-1 0xfffffffe <ok> node-2 0xfffffffe <ok> node-30 0xfffffffe <ok>" --before "running running" --after "configured out"
cargo run --release -- --raw-hpc data/HPC.log --to-parse "inconsistent nodesets node-31 0x1fffffffe <ok> node-0 0xfffffffe <ok> node-1 0xfffffffe <ok> node-2 0xfffffffe <ok> node-30 0xfffffffe <ok>" --before "running running" --after "configured out" --cutoff 106
cargo run --release -- --raw-hpc data/HPC.log --to-parse "58717 2185 boot_cmd new 1076865186 1 Targeting domains:node-D1 and nodes:node-[40-63] child of command 2176" --before-line "58728 2187 boot_cmd new 1076865197 1 Targeting domains:node-D2 and nodes:node-[72-95] child of command 2177" --after-line "58707 2184 boot_cmd new 1076865175 1 Targeting domains:node-D0 and nodes:node-[0-7] child of command 2175" --cutoff 106
cargo run --release -- --raw-proxifier data/Proxifier_2k.log --to-parse "[10.30 16:54:08] chrome.exe - proxy.cse.cuhk.edu.hk:5070 close, 3637 bytes (3.55 KB) sent, 1432 bytes (1.39 KB) received, lifetime 00:01" --before "proxy.cse.cukh.edu.hk:5070 HTTPS" --after "open through" --cutoff 10
cargo run --release -- --raw-healthapp data/HealthApp_2k.log --to-parse "20171223-22:15:41:672|Step_StandReportReceiver|30002312|REPORT : 7028 5017 150539 240" --before "calculateAltitudeWithCache totalAltitude=240" --after "onStandStepChanged 3601"
cargo run --release -- --raw-healthapp data/HealthApp.log --to-parse "20171223-22:15:41:672|Step_StandReportReceiver|30002312|REPORT : 7028 5017 150539 240" --before "calculateAltitudeWithCache totalAltitude=240" --after "onStandStepChanged 3601" --cutoff 10
```

You'll need to untar `OpenStack.tar.gz` to try this one (but it doesn't work well anyway):
```
cargo run --release -- --raw-openstack data/openstack_normal2.log --to-parse "nova-compute.log.2017-05-17_12:02:35 2017-05-17 12:02:30.397 2931 INFO nova.virt.libvirt.imagecache [req-addc1839-2ed5-4778-b57e-5854eb7b8b09 - - - - -] image 0673dd71-34c5-4fbb-86c4-40623fbe45b4 at (/var/lib/nova/instances/_base/a489c868f0c37da93b76227c91bb03908ac0e742): in use: on this node 1 local, 0 on other nodes sharing this instance storage"
```


# Strategy 1: Allow separate maps/threads to run concurrently to improve performance

## Summary

This strategy focused on the function `dictionary_builder_concurrent_map()` and added a new feature to allow starting multiple threads with a single concurrent map to process an input file concurrently. This feature is enabled by **NOT** providing the `--single-map` flag (a.k.a. the default case). The maximal number of threads to start is specified by the `--num-threads` flag. The program will wait for all threads to finish before outputting the results. Note that if `num-threads` is set to 1, then the program will run the original implementation. This strategy is very similar to the previous strategy, except that now all threads use only one map.


## Technical Details

Initialization:
- The function `dictionary_builder_concurrent_map()` will read the input file line by line and save those lines as a vector of strings. It will then divide this vector into chunks of up to `num_threads`, each with the same number of lines (except the last chunk in some cases), so that each chunk can be processed by an independent thread. It is important to note here that the last two tokens in the current chunk will be passed to the next thread, while the first two tokens in the next chunk will be passed to the current thread; this is to ensure that the output of the 2-gram and 3-gram mappings is properly constructed according to the strategy described in the paper.

Thread workflow:
- There are 2 shared concurrent maps (using crate `DashMap`), declared with atomic reference counters (`2-gram map`, `3-gram map`), used to store the results. When the threads run, they will call the function `concurrent_process_dictionary_builder_line()` to process each line assigned to it and store the results in a clone of `Arc` of the concurrent map accordingly.

Finally:
- At the end of the function `dictionary_builder_concurrent_map()`, the main thread will wait for all work threads to finish. The program will then continue to execute other functions that have not changed in this strategy and utilize the results of the shared maps and set computed in this function as before.

## Testing

I want to test 2 main variables: the number of threads and the different styles of input files (e.g. format, number of lines, etc...) . I started by adding some unit tests to the function `parse_raw()` based on the test file `from_paper.log`, which I set up starting with different thread counts. I tested various cases, including cases where the number of threads was well below the number of lines and well above the number of lines. Then I continued this strategy by testing the function `dictionary_builder_concurrent_map()` with different test files and comparing the results to the original file. I wrote a simple python script `correctness_test_script.py` to do this, and I used `compare.py` provided on piazza to compare the results. In the test script, I tested each input file with multiple thread counts and different search criteria. Since I got the correct results in all 27 test cases, I can conclude that the function `dictionary_builder_concurrent_map()` is working as expected.


## Performance

Similar to the testing strategy, I wrote a script `performance_test_script.py` to compare the performance of my implementation with the original implementation. I used `hyperfine` to benchmark the original implementation and my implementation for different number of threads and input files. My implementation has a total of 27 performance tests (9 input files, each with 3 different numbers of threads). Some results are shown below if you are interested.

| Test # / Input File | Original Implementation | Thread Count | My Implementation | Speedup |
| ------------------- | ----------------------- | ------------ | ----------------- | ------- |
| 1 "HDFS_2K.log"     | 225 ms                  | 8            | 213 ms            | ~1.06   |
| 2 "HPC.log"         | 8.7 s                   | 8            | 7.66 s            | ~1.13   |
| 3 "HPC.log"         | 7.9 s                   | 4            | 7.86 s            | ~1      |
| 4 "HPC.log"         | 7.9 s                   | 8            | 7.09 s            | ~1.11   |



# Strategy 2: Allow separate maps/threads to run concurrently to improve performance

## Summary

This strategy focused on the function `dictionary_builder_single_map()` and added a new feature to allow starting multiple threads with separate maps to process an input file concurrently. This feature is enabled by providing the `--single-map` flag. The maximal number of threads to start is specified by the `--num-threads` flag. The program will wait for all threads to finish before outputting the results. Note that if `num-threads` is set to 1, then the program will run the original implementation.


## Technical Details

Initialization:
- The function `dictionary_builder_single_map()` will read the input file line by line and save those lines as a vector of strings. It will then divide this vector into chunks of up to `num_threads`, each with the same number of lines (except the last chunk in some cases), so that each chunk can be processed by an independent thread. It is important to note here that the last two tokens in the current chunk will be passed to the next thread, while the first two tokens in the next chunk will be passed to the current thread; this is to ensure that the output of the 2-gram and 3-gram mappings is properly constructed according to the strategy described in the paper.

Thread workflow:
- There are shared maps and sets declared with atomic reference counters (`2-gram map`, `3-gram map` and `all token set`, I changed the list to a set so that there are no duplicate tokens) that are outside the scope of each thread, and they are used to store the final result. Threads will have clones of these objects, as well as their own corresponding versions of these objects. When threads are running, they will call the function `process_dictionary_builder_line()` to process each line assigned to it and store the result in their own (local) version of said objects. When a thread has finished with all the lines in the chunk that is assigned to it, it will merge its results into the shared map and sets and close. The merging process is done by calling the function `merge_maps()`, which does the addition to the shared maps if the key already exists, and inserts the key and value if the key does not exist.

Finally:
- At the end of the function `dictionary_builder_single_map()`, the main thread will wait for all work threads to finish. The program will then continue to execute other functions that have not changed in this strategy and utilize the results of the shared maps and set computed in this function as before.

## Testing

I want to test 2 main variables: the number of threads and the different styles of input files (e.g. format, number of lines, etc...) . I started by adding some unit tests to the function `parse_raw()` based on the test file `from_paper.log`, which I set up starting with different thread counts. I tested various cases, including cases where the number of threads was well below the number of lines and well above the number of lines. Then I continued this strategy by testing the function `dictionary_builder_single_map()` with different test files and comparing the results to the original file. I wrote a simple python script `correctness_test_script.py` to do this, and I used `compare.py` provided on piazza to compare the results. In the test script, I tested each input file with multiple thread counts and different search criteria. Since I got the correct results in all 27 test cases, I can conclude that the function `dictionary_builder_single_map()` is working as expected.


## Performance

Similar to the testing strategy, I wrote a script `performance_test_script.py` to compare the performance of my implementation with the original implementation. I used `hyperfine` to benchmark the original implementation and my implementation for different number of threads and input files. My implementation has a total of 27 performance tests (9 input files, each with 3 different numbers of threads). Some results are shown below if you are interested.

| Test # / Input File  | Original Implementation | Thread Count | My Implementation | Speedup     |
| -------------------- | ----------------------- | ------------ | ----------------- | ----------- |
| 1 "from_paper.log"   | 120 ms                  | 10           | 142 ms            | no speed up |
| 2 "HealthApp_2K.log" | 209 ms                  | 10           | 247 ms            | no speed up |
| ...                  | ...                     | ...          | ...               | ...         |
| 6 "HDFS_2K.log"      | 225 ms                  | 8            | 213 ms            | ~1.05       |
| 7 "HPC.log"          | 8.6 s                   | 10           | 7.77 s            | ~1.1        |
| ...                  | ...                     | ...          | ...               | ...         |






