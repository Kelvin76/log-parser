#!/usr/bin/python
import sys
import os
from subprocess import call
import re

try:
  from subprocess import DEVNULL
except ImportError:
  import os
  DEVNULL = open(os.devnull, 'wb')

test_count = 0

def check(file_format, file_name, to_parse, before, after, cutoff, thread_counts = [], runs = [0, 0], is_line = False):
    global test_count
    test_count += 1

    before_argument = "--before" + ("-line" if is_line else "")
    after_argument = "--after" + ("-line" if is_line else "")

    # Run the original code
    original_output = "original.out"
    call(["cargo", "run", "--release", "--", file_format, "data/" + file_name + ".log", "--to-parse", to_parse, before_argument, before, after_argument, after, "--cutoff", str(cutoff), "--num-threads", "1", ">", original_output], shell=True)

    # Run my code
    mine_output = "mine.out"

    # Single map
    if runs[0] == 1:
        for thread_count in thread_counts:
            call(["cargo", "run", "--release", "--", file_format, "data/" + file_name + ".log", "--to-parse", to_parse, before_argument, before, after_argument, after, "--cutoff", str(cutoff), "--single-map", "--num-threads", str(thread_count) , ">", mine_output], shell=True)

            # Compare the outputs
            diff_file = f"diff{test_count}_{thread_count}_s.out"
            call(["python3", "compare.py", original_output, mine_output, ">", diff_file], shell=True)

            # Check if the outputs are the same
            match_str = "Files match within specified tolerance\n"
            print(f"==== Single Map Correctness Test - {test_count}_{thread_count} - ", end = "")
            with open(diff_file, "r") as f:
                if f.readline() == match_str:
                    print(f"Matches! Y")
                    call(["rm", diff_file], shell=True) # Delete the diff file if the test passed
                else:
                    print(f"Failed! X")
            print(f"     This test is - format:     {file_format}")
            print(f"                    file:       {file_name}")
            print(f"                    thread cnt: {thread_count}")
            print(f"                    test info: Single map")

    # Concurrent map
    if runs[1] == 1:
        for thread_count in thread_counts:
            call(["cargo", "run", "--release", "--", file_format, "data/" + file_name + ".log", "--to-parse", to_parse, before_argument, before, after_argument, after, "--cutoff", str(cutoff), "--num-threads", str(thread_count) , ">", mine_output], shell=True)

            # Compare the outputs
            diff_file = f"diff{test_count}_{thread_count}_c.out"
            call(["python3", "compare.py", original_output, mine_output, ">", diff_file], shell=True)

            # Check if the outputs are the same
            match_str = "Files match within specified tolerance\n"
            print(f"==== Concurrent Correctness Test - {test_count}_{thread_count} - ", end = "")
            with open(diff_file, "r") as f:
                if f.readline() == match_str:
                    print(f"Matches! Y")
                    call(["rm", diff_file], shell=True) # Delete the diff file if the test passed
                else:
                    print(f"Failed! X")
            print(f"     This test is - format:     {file_format}")
            print(f"                    file:       {file_name}")
            print(f"                    thread cnt: {thread_count}")
            print(f"                    test info: Concurrent map")

    # Clean up
    call(["rm", original_output, mine_output], shell=True)

if __name__ == "__main__":
    thread_counts = [5, 10]
    runs = [0, 1]   # Run single map, concurrent map

    all_tests = [
        ["--raw-spark", "from_paper", "17/06/09 20:11:11 INFO storage.BlockManager: Found block rdd_42_20 locally", "split: hdfs://hostname/2kSOSP.log:29168+7292", "Found block", 3, thread_counts, runs],
        ["--raw-hdfs", "HDFS_2k", "081109 204925 673 INFO dfs.DataNode$DataXceiver: Receiving block blk_-5623176793330377570 src: /10.251.75.228:53725 dest: /10.251.75.228:50010", "size <*>", "BLOCK* NameSystem.allocateBlock:", 1, thread_counts, runs],
        ["--raw-healthapp", "HealthApp_2k", "20171223-22:15:41:672|Step_StandReportReceiver|30002312|REPORT : 7028 5017 150539 240", "calculateAltitudeWithCache totalAltitude=240","onStandStepChanged 3601" , 3, thread_counts, runs],
        ["--raw-healthapp", "HealthApp", "20171223-22:15:41:672|Step_StandReportReceiver|30002312|REPORT : 7028 5017 150539 240", "calculateAltitudeWithCache totalAltitude=240","onStandStepChanged 3601" , 10, thread_counts, runs],
        ["--raw-hpc", "HPC_2k", "inconsistent nodesets node-31 0x1fffffffe <ok> node-0 0xfffffffe <ok> node-1 0xfffffffe <ok> node-2 0xfffffffe <ok> node-30 0xfffffffe <ok>", "running running","configured out" , 3, thread_counts, runs],
        ["--raw-hpc", "HPC", "inconsistent nodesets node-31 0x1fffffffe <ok> node-0 0xfffffffe <ok> node-1 0xfffffffe <ok> node-2 0xfffffffe <ok> node-30 0xfffffffe <ok>", "running running","configured out" , 106, thread_counts, runs],
        ["--raw-hpc", "HPC", "58717 2185 boot_cmd new 1076865186 1 Targeting domains:node-D1 and nodes:node-[40-63] child of command 2176", "58728 2187 boot_cmd new 1076865197 1 Targeting domains:node-D2 and nodes:node-[72-95] child of command 2177", "58707 2184 boot_cmd new 1076865175 1 Targeting domains:node-D0 and nodes:node-[0-7] child of command 2175", 106, thread_counts, runs, True],
        ["--raw-linux", "Linux_2k.", "Jun 23 23:30:05 combo sshd(pam_unix)[26190]: authentication failure; logname= uid=0 euid=0 tty=NODEVssh ruser= rhost=218.22.3.51  user=root", "rhost=<*> user=root", "session opened", 100, thread_counts, runs],
        ["--raw-proxifier", "Proxifier_2k", "[10.30 16:54:08] chrome.exe - proxy.cse.cuhk.edu.hk:5070 close, 3637 bytes (3.55 KB) sent, 1432 bytes (1.39 KB) received, lifetime 00:01", "proxy.cse.cukh.edu.hk:5070 HTTPS","open through" , 10, thread_counts, runs],
    ]
    for test in all_tests:
        check(*test)

