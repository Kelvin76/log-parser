#!/usr/bin/python
import os
from subprocess import call

try:
    from subprocess import DEVNULL
except ImportError:
    import os

    DEVNULL = open(os.devnull, "wb")

test_count = 0


def check(
    file_format,
    file_name,
    to_parse,
    before,
    after,
    cutoff,
    thread_counts,
    is_line=False,
):
    global test_count
    test_count += 1

    before_argument = "--before" + ("-line" if is_line else "")
    after_argument = "--after" + ("-line" if is_line else "")

    print(f"==== Performance Test - {test_count} -----------------")

    command = f'cargo run --release -- "{file_format}" data/{file_name}.log --to-parse "{to_parse}" {before_argument} "{before}" {after_argument} "{after}" --cutoff {cutoff}'
    original_command = command + f" --num-threads 1"

    # Run the original version
    call(["hyperfine", original_command])

    for thread_count in thread_counts:
        concurrent_map_impl_command = command + f" --num-threads {thread_count}"
        single_map_impl_command = f"{concurrent_map_impl_command} --single-map"

        # Run the single map version
        call(["hyperfine", single_map_impl_command])

        # Run the concurrent version
        call(["hyperfine", concurrent_map_impl_command])

    print("\n\n")


if __name__ == "__main__":
    thread_counts = [2, 4, 8]

    all_tests = [
        [
            "--raw-hdfs",
            "HDFS_2k",
            "081109 204925 673 INFO dfs.DataNode$DataXceiver: Receiving block blk_-5623176793330377570 src: /10.251.75.228:53725 dest: /10.251.75.228:50010",
            "size <*>",
            "BLOCK* NameSystem.allocateBlock:",
            1,
            thread_counts,
        ],
        [
            "--raw-hpc",
            "HPC",
            "inconsistent nodesets node-31 0x1fffffffe <ok> node-0 0xfffffffe <ok> node-1 0xfffffffe <ok> node-2 0xfffffffe <ok> node-30 0xfffffffe <ok>",
            "running running",
            "configured out",
            106,
            thread_counts,
        ],
        [
            "--raw-hpc",
            "HPC",
            "58717 2185 boot_cmd new 1076865186 1 Targeting domains:node-D1 and nodes:node-[40-63] child of command 2176",
            "58728 2187 boot_cmd new 1076865197 1 Targeting domains:node-D2 and nodes:node-[72-95] child of command 2177",
            "58707 2184 boot_cmd new 1076865175 1 Targeting domains:node-D0 and nodes:node-[0-7] child of command 2175",
            106,
            thread_counts,
            True,
        ],
        [
            "--raw-healthapp",
            "HealthApp",
            "20171223-22:15:41:672|Step_StandReportReceiver|30002312|REPORT : 7028 5017 150539 240",
            "calculateAltitudeWithCache totalAltitude=240",
            "onStandStepChanged 3601",
            10,
            thread_counts,
        ],
        [
            "--raw-hpc",
            "HPC_2k",
            "inconsistent nodesets node-31 0x1fffffffe <ok> node-0 0xfffffffe <ok> node-1 0xfffffffe <ok> node-2 0xfffffffe <ok> node-30 0xfffffffe <ok>",
            "running running",
            "configured out",
            3,
            thread_counts,
        ],
        [
            "--raw-linux",
            "Linux_2k.",
            "Jun 23 23:30:05 combo sshd(pam_unix)[26190]: authentication failure; logname= uid=0 euid=0 tty=NODEVssh ruser= rhost=218.22.3.51  user=root",
            "rhost=<*> user=root",
            "session opened",
            100,
            thread_counts,
        ],
        [
            "--raw-proxifier",
            "Proxifier_2k",
            "[10.30 16:54:08] chrome.exe - proxy.cse.cuhk.edu.hk:5070 close, 3637 bytes (3.55 KB) sent, 1432 bytes (1.39 KB) received, lifetime 00:01",
            "proxy.cse.cukh.edu.hk:5070 HTTPS",
            "open through",
            10,
            thread_counts,
        ],
    ]
    for test in all_tests:
        check(*test)
